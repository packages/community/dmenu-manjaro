# Maintainer: Bernhard Landauer <oberon@manjaro.org>
# Contributor: Chrysostomus @forum.manjaro.org
# Contributor: Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor: Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor: Thorsten Töpper <atsutane-tu@freethoughts.de>
# Contributor: Thayer Williams <thayer@archlinux.org>
# Contributor: Jeff 'codemac' Mickey <jeff@archlinux.org>
# Contributor: Jonathon Fernyhough <jonathon at manjaro_dot_org>
# Contributor: Indian Sunset @idnsusnet

pkgname=dmenu-manjaro
_pkgname=dmenu
pkgver=5.0
pkgrel=2
pkgdesc="customized recency-aware dmenu for manjaro-i3 with xft- and mouse-support"
arch=('x86_64' 'aarch64')
groups=('i3-manjaro')
license=('MIT')
depends=('coreutils'
         'fontconfig'
         'freetype2'
         'glibc'
         'libfontconfig.so'
         'libx11'
         'libxinerama'
         'libxft'
         'libxinerama'
         'noto-fonts'
         'sh')
conflicts=("$_pkgname" "$_pkgname-xft")
provides=("$_pkgname" "$_pkgname-xft")
install=$pkgname.install
source=("https://dl.suckless.org/tools/$_pkgname-$pkgver.tar.gz"
        'dmenu-mousesupport-5.0.diff'
        'dmenu-lineheight-4.9.diff'
        'dmenu-xyw-4.8.diff'
        'dmenu_recency'
        'dmenurc')
sha256sums=('fe18e142c4dbcf71ba5757dbbdea93b1c67d58fc206fc116664f4336deef6ed3'
            '06a8c861b0745bd8bf2ccd011f9e5bc209841f979cfae0fb0ce4e5e97bf33146'
            '4f7f6f46b23cbc3f38bcc7eff24a0cb74b8bc8651c2de29aa3adc4f16b4dc06c'
            '63cb0925f0984c62fa4e0943f5d1463c740a14e58a7596cc326e8c51b4b6592b'
            '6dc85c9245a8372ae4a6e4f5f95b8f9431dad67b53961857571b2226feacaafc'
            'c0b773af5a1458036d242b19f2e45ed444e6e86d2cf15bedbfb996e8c2fd05b6')

prepare() {
    cd $_pkgname-$pkgver
    echo "CPPFLAGS+=${CPPFLAGS}" >> config.mk
    echo "CFLAGS+=${CFLAGS}" >> config.mk
    echo "LDFLAGS+=${LDFLAGS}" >> config.mk

    msg "add mouse support"
    patch -p1 < ../dmenu-mousesupport-5.0.diff
    msg "line-height patch"
    patch -p1 < ../dmenu-lineheight-4.9.diff
    msg "xyw patch"
    patch -p1 < ../dmenu-xyw-4.8.diff
}
build() {
	cd $_pkgname-$pkgver
	make X11INC=/usr/include/X11 X11LIB=/usr/lib/X11 FREETYPEINC=/usr/include/freetype2
}

package() {
	cd $_pkgname-$pkgver
	make PREFIX=/usr DESTDIR="$pkgdir" install
	install -Dm755 "$srcdir/dmenu_recency" "$pkgdir/usr/bin/dmenu_recency"
	install -Dm755 "$srcdir/dmenurc" "$pkgdir/usr/share/$_pkgname/dmenurc"
    install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}
